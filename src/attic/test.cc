#include <stdio.h>

#include "rbn_solve.h"

#include <SDL/SDL.h>
#include <unistd.h>
#include <set>
#include <stdio.h>

struct XY {
    double x, y;
    XY (double xx, double yy) : x(xx), y(yy) {}
    bool operator<(const XY &rhs) const {return x < rhs.x;}
};

typedef std::set<XY> POINTS;

void make(const POINTS &p, double **xs, double **ys) {
    *xs = new double[p.size()];
    *ys = new double[p.size()];
    double *xp = *xs;
    double *yp = *ys;
    const POINTS::const_iterator end = p.end();
    for (POINTS::const_iterator pp = p.begin(); pp != end; ++pp) {
        *xp++ = pp->x;
        *yp++ = pp->y;
    }
}

enum {
    WIDTH = 1024,
    HEIGHT = 768,
    BPP = 24,
    EVERY = 4
};

enum {
    WEVERY = WIDTH/EVERY,
    HEVERY = HEIGHT/EVERY
};



static int TARGET_SCREEN_WIDTH = WIDTH;
static int TARGET_SCREEN_HEIGHT = HEIGHT;
static int TARGET_SCREEN_BPP = BPP;
static double SIGMA = 4.0;

SDL_Surface *surf;

static SDL_Surface *makeSurface() {
    int videoFlags = 0;                  // Flags to pass to SDL_SetVideoMode
    const SDL_VideoInfo *videoInfo;	 // this holds some info about our display
    SDL_Surface *ret;

    /* initialize SDL */
    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        fprintf( stderr, "Video initialization failed: %s\n", SDL_GetError( ) );
	return 0;
    }
    if (!(videoInfo = SDL_GetVideoInfo( ))) {
        fprintf( stderr, "Video query failed: %s\n", SDL_GetError( ) );
        return 0;
    }
    /* the flags to pass to SDL_SetVideoMode */
//    videoFlags  = SDL_OPENGL;          /* Enable OpenGL in SDL */
//    videoFlags |= SDL_GL_DOUBLEBUFFER; /* Enable double buffering */
    videoFlags |= SDL_HWPALETTE;       /* Store the palette in hardware */
//    videoFlags |= SDL_RESIZABLE;       /* Enable window resizing */
//    if (TRY_FULLSCREEN)
//    videoFlags |= SDL_FULLSCREEN;

    /* This checks to see if surfaces can be stored in memory */
    videoFlags |= videoInfo->hw_available ? SDL_HWSURFACE : SDL_SWSURFACE;
    /* This checks if hardware blits can be done */
    if (videoInfo->blit_hw) videoFlags |= SDL_HWACCEL;
    /* Sets up OpenGL double buffering */
//    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
    
    /* Set up FSAA */
    /*
    if (RConfig::TARGET_FSAA > 0) {    
        int fsaa = RConfig::TARGET_FSAA;
        while (fsaa > 0 && SDL_GL_SetAttribute (SDL_GL_SAMPLES_SIZE, fsaa)
            --fsaa;
        fprintf(stderr, "Using %dx FSAA\n", fsaa);
    }*/
    
    /* get a SDL surface */
    ret = SDL_SetVideoMode( TARGET_SCREEN_WIDTH, TARGET_SCREEN_HEIGHT, TARGET_SCREEN_BPP, videoFlags );
    if (!ret) {
        fprintf( stderr,  "Video mode set failed: %s\n", SDL_GetError( ) );
	return 0;
    }
    if (videoFlags & SDL_HWSURFACE) {
        fprintf(stderr, "Using hardware accelleration\n");
    }
    return ret;
}

static void draw(const POINTS &ps);

static void eventLoop() {
    SDL_Event event;
    POINTS ps;
    bool terminate = false;
    while (!terminate && SDL_WaitEvent(&event)) {
        switch(event.type) {
        case SDL_QUIT:
            terminate = 1;
            break;
        case SDL_MOUSEBUTTONDOWN: break;
        case SDL_MOUSEBUTTONUP:
            ps.insert(XY(event.button.x, event.button.y));
            fprintf(stderr, "Added point (%d, %d)\n", event.button.x, event.button.y);
            if (ps.size() > 8)
                draw(ps);
            break;
        case SDL_MOUSEMOTION:
        case SDL_KEYUP:
        case SDL_KEYDOWN:
            break;
        case SDL_USEREVENT:
            break;
        default:
            break;
        }
    }
}

static void draw (const POINTS &ps) {
    SDL_LockSurface(surf);

    static const Uint32 BLACK = SDL_MapRGB(surf->format, 0, 0, 0);
    static const Uint32 CYAN = SDL_MapRGB(surf->format, 0, 255, 255);

    XYInterp net(SIGMA);
    double pts[WEVERY][HEVERY];

    const POINTS::const_iterator end = p.end();
    for (POINTS::const_iterator pp = p.begin(); pp != end; ++pp) {
        XYInterp::EXAMPLE(
        net.addExample(
    }

    SDL_Rect col;
    int tr, tc;
    col.w = 1;
    col.h = surf->h;
    col.y = 0;

    SDL_Rect pix;
    pix.w = 1;
    pix.h = 1;
    pix.x = col.x;


    for (col.x = x0; col.x <= xn; ++col.x) {
        SDL_FillRect(surf, &col, reinterpret_cast<Uint32>(BLACK));
        fprintf(stderr, "(%f, %f)\n", xo[col.x-x0], yo[col.x-x0]);
        if (yo[col.x-x0] > 0.0 && yo[col.x-x0] < surf->h) {
            pix.x = col.x;
            pix.y = static_cast<int>(yo[col.x-x0] + 0.5);
            SDL_FillRect(surf, &pix, reinterpret_cast<Uint32>(CYAN));
        }
    }

    col.x = 0;
    col.w = surf->w;

    SDL_UnlockSurface(surf);
    SDL_UpdateRects(surf, 1, &col);
    delete xo;
    delete yo;
    delete xs;
    delete ys;

}


int main(int argc, char **argv) {
    atexit(&SDL_Quit);
    if (argc > 2) {
        TARGET_SCREEN_WIDTH = atoi(argv[1]);
        TARGET_SCREEN_HEIGHT = atoi(argv[2]);
    }
    if (TARGET_SCREEN_WIDTH <= 0 ||
        TARGET_SCREEN_HEIGHT <= 0) {
        fprintf(stderr, "Usage: %s [<width> <height>]");
        return 1;
    }
    surf = makeSurface();

    if (!surf) {
        return 2;
    }

    eventLoop();

    return 0;
}
