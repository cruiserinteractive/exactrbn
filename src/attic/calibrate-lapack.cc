#include <SDL/SDL.h>
#include <SDL/SDL_thread.h>

#include <unistd.h>

/*
************************************************************************
*  lineq.c: Solve matrix equation Ax=b using LAPACK routine sgesv      *
*								       *
*  taken from: "Projects in Computational Physics" by Landau and Paez  *
*	       copyrighted by John Wiley and Sons, New York            *
*                                                                      *
*  written by: students in PH465/565, Computational Physics,           *
*	       at Oregon State University                              *
*              code copyrighted by RH Landau                           *
*  supported by: US National Science Foundation, Northwest Alliance    *
*                for Computational Science and Engineering (NACSE),    *
*                US Department of Energy 	                       *
*								       *
*  UNIX (IBM AIX): cc -c lineq.c                                       *
*	           f77 lineq.o -lblas -llapack   		       *
*  UNIX (DEC OSF): rename sgesv to sgesv_                              *
*                  cc lineq.c -ldxml                                   *
*                                                                      *
*  comment: LAPACK has to be installed on your system                  *
************************************************************************
*/

#include<stdio.h>

extern "C" {
    int sgesv_(int &a_size,
              int &c2,
              float *AT,
              int &pivot_size,
              int *pivot,
              float *rhs,
              int &b_size,
              int &ok);

    int dgesv_(int &a_size,
              int &c2,
              double *AT,
              int &pivot_size,
              int *pivot,
              double *rhs,
              int &b_size,
              int &ok);
}

main() {
    enum {size = 3};
    int i, j , c1, c2, pivot[size], ok;

    double A[size][size], b[size], AT[size*size];

    A[0][0] =  1.0; A[0][1] = -0.2; A[0][2] = -0.1;        /* matrix A */
    A[1][0] = -0.4; A[1][1] =  1.0; A[1][2] = -0.7;
    A[2][0] = -0.4; A[2][1] =  1.0; A[2][2] = -0.7;

    b[0] = -1;                                          /* vector b */
    b[1] = 1;
    b[2] = -1;

    for (i=0; i<size; i++)	     	/* transform the matrix so */
    {                                    /* we can pass it to Fortran */
        for(j = 0 ; j < size ; j += 1) AT[j + size*i] = A[j][i];
    }

    c1 = size; 		/* define variable so we can pass pointer */
    c2 = 1;              /* to these variables to the routine */

    dgesv_(c1, c2, AT, c1, pivot, b, c1, ok);    /* sgesv_ for DEC */
    /* parameters in the order as they appear in the function call: */
      /* order of matrix A, number of right hand sides (b), matrix A, */
      /* leading dimension of A, array records pivoting, */
      /* result vector b on entry, x on exit, leading dimension of b */
      /* return value =0 for success*/

   if (!ok)
   {
      for (j=0; j<size; j++) printf("%f\n", b[j]);	/* print x */
   }
   else printf("An error occurred\n");
}

#ifdef MOOMOOMOO
static int TARGET_SCREEN_WIDTH = 1024;
static int TARGET_SCREEN_HEIGHT = 768;
static int BOX_SIZE = 20;
static int TARGET_SCREEN_BPP = 24;
static float FROM_EDGE = 0.15;

static const char * fname = "calibrate.dat";

SDL_Surface *surf;

class RBNWrap : public annie::RadialBasisNetwork {
public:
    RBNWrap(int inputs, int centers, int outputs)
        :
    RadialBasisNetwork(inputs, centers, outputs){}

    RBNWrap(const char *filename)
        :
    RadialBasisNetwork(filename){}

    virtual ~RBNWrap() {}

    virtual void save(const std::string &s) {
        save(s.c_str());
    }
    virtual annie::Vector getOutput(const annie::Vector & v) {
        annie::Vector vv(v);
        return getOutput(vv);
    }
};


static SDL_Surface *makeSurface() {
    int videoFlags = 0;                  // Flags to pass to SDL_SetVideoMode
    const SDL_VideoInfo *videoInfo;	 // this holds some info about our display
    SDL_Surface *ret;

    /* initialize SDL */
    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        fprintf( stderr, "Video initialization failed: %s\n", SDL_GetError( ) );
	return 0;
    }
    if (!(videoInfo = SDL_GetVideoInfo( ))) {
        fprintf( stderr, "Video query failed: %s\n", SDL_GetError( ) );
        return 0;
    }
    /* the flags to pass to SDL_SetVideoMode */
//    videoFlags  = SDL_OPENGL;          /* Enable OpenGL in SDL */
//    videoFlags |= SDL_GL_DOUBLEBUFFER; /* Enable double buffering */
    videoFlags |= SDL_HWPALETTE;       /* Store the palette in hardware */
//    videoFlags |= SDL_RESIZABLE;       /* Enable window resizing */
//    if (TRY_FULLSCREEN)
    videoFlags |= SDL_FULLSCREEN;

    /* This checks to see if surfaces can be stored in memory */
    videoFlags |= videoInfo->hw_available ? SDL_HWSURFACE : SDL_SWSURFACE;
    /* This checks if hardware blits can be done */
    if (videoInfo->blit_hw) videoFlags |= SDL_HWACCEL;
    /* Sets up OpenGL double buffering */
//    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

    /* Set up FSAA */
    /*
    if (RConfig::TARGET_FSAA > 0) {
        int fsaa = RConfig::TARGET_FSAA;
        while (fsaa > 0 && SDL_GL_SetAttribute (SDL_GL_SAMPLES_SIZE, fsaa)
            --fsaa;
        fprintf(stderr, "Using %dx FSAA\n", fsaa);
    }*/

    /* get a SDL surface */
    ret = SDL_SetVideoMode( TARGET_SCREEN_WIDTH, TARGET_SCREEN_HEIGHT, TARGET_SCREEN_BPP, videoFlags );
    if (!ret) {
        fprintf( stderr,  "Video mode set failed: %s\n", SDL_GetError( ) );
	return 0;
    }
    if (videoFlags & SDL_HWSURFACE) {
        fprintf(stderr, "Using hardware accelleration\n");
    }
    return ret;
}



using annie::Vector;
using annie::TrainingSet;

static void clearTouch() {}
static void getTouch(int &row, int &col) {
    SDL_Event event;
    bool terminate = false;
    while (!terminate && SDL_WaitEvent(&event)) {
        switch(event.type) {
        case SDL_QUIT:
            terminate = 1;
            break;
        case SDL_MOUSEBUTTONDOWN: break;
        case SDL_MOUSEBUTTONUP:
            //terminate = handleButton(event.button);
            col = event.button.x;
            row = event.button.y;
            terminate = true;
            break;
        case SDL_MOUSEMOTION:
        case SDL_KEYUP:
        case SDL_KEYDOWN:
            break;
        case SDL_USEREVENT:
            break;
        default:
            break;
        }
    }
}


void doPoint (const double x, const double y, TrainingSet &ts) {
    SDL_LockSurface(surf);

    const Uint32 BLACK = SDL_MapRGB(surf->format, 0, 0, 0);
    const Uint32 CYAN = SDL_MapRGB(surf->format, 0, 255, 255);


    SDL_Rect r;
    int tr, tc;
    r.w = BOX_SIZE;
    r.h = BOX_SIZE;

    Vector vin(2), vout(2);

    vout[0] = r.x = static_cast<Sint16>(x - (BOX_SIZE / 2) + 0.5);
    vout[1] = r.y = static_cast<Sint16>(y - (BOX_SIZE / 2) + 0.5);

    SDL_FillRect(surf, &r, reinterpret_cast<Uint32>(CYAN));
    SDL_UnlockSurface(surf);
    SDL_UpdateRects(surf, 1, &r);

    clearTouch();
    getTouch(tr, tc);

    vin[0] = tr;
    vin[1] = tc;

    ts.addIOpair(vin, vout);

    usleep(1000000);
    SDL_LockSurface(surf);
    SDL_FillRect(surf, &r, (Uint32)BLACK);
    SDL_UnlockSurface(surf);
    SDL_UpdateRects(surf, 1, &r);
}

int replay() {
    RBNWrap rbn(fname);
    Vector vin(2), vout(2);
    enum {SZ = 10};
    int i = 0;
    SDL_Rect r = {SZ, SZ, SZ, SZ};

    SDL_Event event;
    bool terminate = false;
    while (!terminate && SDL_WaitEvent(&event)) {
        switch(event.type) {
        case SDL_QUIT:
            terminate = 1;
            break;
        case SDL_MOUSEBUTTONDOWN:
        case SDL_MOUSEBUTTONUP:
            //terminate = handleButton(event.button);
            break;
        case SDL_MOUSEMOTION:
            ++i;
            vin[0] = event.motion.x;
            vin[1] = event.motion.y;
            vout = rbn.getOutput(vin);
            r.x = static_cast<Sint16>(vout[0] - SZ*0.5 + 0.5);
            r.y = static_cast<Sint16>(vout[1] - SZ*0.5 + 0.5);
            SDL_LockSurface(surf);
            SDL_FillRect(surf, &r, SDL_MapRGB(surf->format, i%256, 255-i%256, (i%128) *2));
            SDL_UnlockSurface(surf);
            SDL_UpdateRects(surf, 1, &r);
            break;
        case SDL_KEYUP:
        case SDL_KEYDOWN:
            terminate = true; //handleKey(event.key);
            break;
        case SDL_USEREVENT:
            //terminate = handleUser(event.user);
            break;
        default:
            break;
        }
    }
    return 0;
}

int main(int argc, char **argv) {
    atexit(&SDL_Quit);
    if (argc > 2) {
        TARGET_SCREEN_WIDTH = atoi(argv[1]);
        TARGET_SCREEN_HEIGHT = atoi(argv[2]);
    }
    if (TARGET_SCREEN_WIDTH <= 0 ||
        TARGET_SCREEN_HEIGHT <= 0) {
        fprintf(stderr, "Usage: %s [<width> <height>]");
        return 1;
    }
    surf = makeSurface();

    if (!surf) {
        return 2;
    }

    usleep(5000000);

    if (argc > 1 && strcmp(argv[1], "-t") == 0)
        return replay();

    /* four corners */
    TrainingSet ts(2, 2);
    doPoint(surf->w*FROM_EDGE, surf->h*FROM_EDGE, ts);
    doPoint(surf->w - surf->w*FROM_EDGE, surf->h*FROM_EDGE, ts);
    doPoint(surf->w - surf->w*FROM_EDGE, surf->h - surf->h*FROM_EDGE, ts);
    doPoint(surf->w*FROM_EDGE, surf->h - surf->h*FROM_EDGE, ts);

    /* midpoints */
    doPoint(surf->w*FROM_EDGE, surf->h*0.5, ts);
    doPoint(surf->w*0.5, surf->h*FROM_EDGE, ts);
    doPoint(surf->w - surf->w*FROM_EDGE, surf->h*0.5, ts);
    doPoint(surf->w*0.5, surf->h - surf->h*FROM_EDGE, ts);

    /* centre */
    doPoint(surf->w*0.5, surf->h*0.5, ts);

    try {
        RBNWrap rbn(2, ts.getSize(), 2);
        rbn.trainWeights(ts);
        rbn.save(fname);
    } catch (annie::Exception &e) {
        std::cerr << e.what() << std::endl;
    }

}

#endif
