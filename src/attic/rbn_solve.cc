/* $Header: /home/tapted/cvstemp/cvs/exactrbn/src/attic/rbn_solve.cc,v 7.2 2005/03/08 04:53:44 tapted Exp $ */
#include "rbn_solve.h"

/**\file rbn_solve.cc
 *
 */
#include "lapack_leqn.h"

#include <stdio.h>
#include <math.h>

static inline double gauss(const double sigma, const double distsq)
__attribute__((const, fastcall, nothrow));

static inline float gauss(const float sigma, const float distsq)
__attribute__((const, fastcall, nothrow));

template <class T, int DIM>
static inline T gdist(const T *const v1, const T *const v2, const T sigma)
__attribute__((regparm(3),
               nonnull,
               nothrow,
               pure));

static inline double gauss(const double sigma, const double distsq) {
    return exp((distsq)/(2.0*sigma));
}

static inline float gauss(const float sigma, const float distsq) {
    return expf((distsq)/(2.0*sigma));
}

template <class T, int DIM>
static inline T gdist(const T *const v1, const T *const v2, const T sigma) {
    T sum = 0;
    for (unsigned i = 0; i < DIM; ++i) {
        const T d = v1[i] - v2[i];
        sum += d*d;
    }
    return gauss(sigma, sum);
}

template <class T, int INDIM, int OUTDIM>
void ExactRBFNet<T, INDIM, OUTDIM>::addExample(const EXAMPLE &ex) {
    examples.push_back(ex);
}

template <class T, int INDIM, int OUTDIM>
int ExactRBFNet<T, INDIM, OUTDIM>::solve() {
    int sz = examples.size();
    int oks = 0;
    T *A = new T[sz*sz];
    T *R = new T[sz];
    for (int r = 0; r < sz; ++r) {
        int c = 0;
        for (; c < r; ++c) {
            A[r*sz + c] = gdist<INDIM>(examples[r].in.v, examples[c].in.v, sigma);
        }
        A[r*sz + c++] = 0;
        for (; c < sz; ++c) {
            A[r*sz + c] = gdist<INDIM>(examples[r].in.v, examples[c].in.v, sigma);
        }
    }
    for (int o = 0; o < OUTDIM; ++o) {
        for (int r = 0; r < sz; ++r)
            R[r] = examples[r].out[o];
        oks += lapack_solve(R, A, sz);
        weights[o] = valarray<T>(R, sz);
    }
    delete[] R;
    delete[] A;
    return oks;
}

template <class T, int INDIM, int OUTDIM>
void ExactRBFNet<T, IMDIM, OUTDIM>::sub(OUTVEC &out, const INVEC &in) const {
    int sz = examples.size();
    for (int o = 0; o < OUTDIM; ++o) {
        out.v[o] = weights[o][0] * gdist<INDIM>(in.v, examples[0].in.v, sigma);
        for (int i = 1; i < sz; ++i) {
            out.v[o] += weights[o][i] * gdist<INDIM>(in.v, examples[i].in.v, sigma);
        }
    }
}
