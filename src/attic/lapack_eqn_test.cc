#include "lapack_leqn.h"

#include <stdio.h>



int main() {
    const double A[] = {
        1, -0.2, -0.1,
        -0.4, 1, -0.7,
        -0.5, -0.3, 1
    };
    double R[] = {
        1, 0, 0
    };
    int res = lapack_solve(R, A, 3);
    printf("(%f, %f, %f), %d\n", R[0], R[1], R[2], res);
}
