/* $Header: /home/tapted/cvstemp/cvs/exactrbn/src/calibrate.cc,v 7.2 2005/03/08 04:53:43 tapted Exp $ */

/**\file calibrate.cc
 * A screen calibration program using / demonstrator of
 * an exact-interpolation radial basis function neural network.
 *
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * \version $Revision: 7.2 $
 * \date $Date$
 */


#include <SDL/SDL.h> //for SDL_Surface, events, etc.
#include <unistd.h>  //for size_t, usleep

#include <deque>    //for points queue
#include <vector>
#include <utility>  //for std::pair
#include <fstream>
#include <iostream>


/** #define VDEBUG for verbose debugging in rbn_solve.h */
//#define VDEBUG
//#define DEBUG
#include "rbn_solve.h"

using std::make_pair;

/** Configuration namespace */
namespace Config {
    int TARGET_SCREEN_WIDTH = 1024; ///< Width of the SDL surface
    int TARGET_SCREEN_HEIGHT = 768; ///< Height of the SDL Surface
    int BOX_SIZE = 20;      ///< Width of the calibration squares
    int TARGET_SCREEN_BPP = 24; ///< Colour depth of the SDL Surface
    int DIFF = 32;          ///< Distance between calibration squares
    float FROM_EDGE = 0.05; ///< Portion from edge of screen at which calibrations squares are drawn
    bool FULLSCREEN = true; ///< Whether to enter fullscreen mode
    bool SHOWPIXELS = true; ///< Whether to show mouse movement events to stderr

    int MAXDIM = 9;         ///< Maximum number of calibration points
    double SIGMA = 10;      ///< Value of sigma for the RBF Network
}
using namespace Config;

/** define LATTICEPATTERN to produce a line grid, rather than dots */
#define LATTICEPATTERN

static SDL_Surface *surf; ///< The SDL surface / screen display handle

static SDL_Surface *makeSurface(); ///< startup function -- load window


/** does nothing..? */
static void clearTouch() {}
/** return/assign the next emulated touch */
static void getTouch(int &row, int &col) {
    SDL_Event event;
    bool terminate = false;
    while (!terminate && SDL_WaitEvent(&event)) {
        switch(event.type) {
        case SDL_QUIT:
            exit(1);
        case SDL_MOUSEBUTTONDOWN: break;
        case SDL_MOUSEBUTTONUP:
            //terminate = handleButton(event.button);
            col = event.button.x;
            row = event.button.y;
            terminate = true;
            break;
        case SDL_MOUSEMOTION:
            if (SHOWPIXELS)
                fprintf(stderr, "\033[`(%d, %d)    ", event.motion.x, event.motion.y);
        case SDL_KEYUP:
            if (event.key.keysym.sym == SDLK_ESCAPE)
                exit(1);
        case SDL_KEYDOWN:
            break;
        case SDL_USEREVENT:
            break;
        default:
            break;
        }
    }
}

/** Add a point to the callibration */
static void doPoint (const double x, const double y, XYInterp &ts) {
    SDL_LockSurface(surf);

    static std::vector<SDL_Rect> points; ///< Remeber past points for display

    const Uint32 BLACK = SDL_MapRGB(surf->format, 0, 0, 0);
    const Uint32 CYAN = SDL_MapRGB(surf->format, 0, 255, 255);
    const Uint32 YELLOW = SDL_MapRGB(surf->format, 255, 255, 0);
    const Uint32 RED = SDL_MapRGB(surf->format, 255, 0, 255);

    SDL_Rect r;
    int tr, tc;
    r.w = BOX_SIZE;
    r.h = BOX_SIZE;

    XYInterp::EXAMPLE ex;

    ex.out[0] = 1.0 * static_cast<Sint16>(x + 0.5) / surf->w;
    ex.out[1] = 1.0 * static_cast<Sint16>(y + 0.5) / surf->h;

    SDL_Rect rr;
    rr.x = static_cast<Sint16>(x + 0.5) - 1;
    rr.y = static_cast<Sint16>(y + 0.5) - 1;
    rr.w = 3;
    rr.h = 3;

    r.x = static_cast<Sint16>(x - (BOX_SIZE / 2) + 0.5);
    r.y = static_cast<Sint16>(y - (BOX_SIZE / 2) + 0.5);

    SDL_FillRect(surf, &r, reinterpret_cast<Uint32>(CYAN));
    for (std::vector<SDL_Rect>::iterator it = points.begin(); it!=points.end();++it)
        SDL_FillRect(surf, &*it, reinterpret_cast<Uint32>(RED));
    SDL_UnlockSurface(surf);
    r.x = r.y = 0;
    r.w = surf->w;
    r.h = surf->h;
    SDL_UpdateRects(surf, 1, &r);
    //SDL_UpdateRects(surf, 1, &r);

    points.push_back(rr);

    clearTouch();
    getTouch(tr, tc);

    ex.in[0] = 1.0 * tc / surf->w;
    ex.in[1] = 1.0 * tr / surf->h;

    //usleep(1000000);
    r.x = r.y = 0;
    r.w = surf->w;
    r.h = surf->h;
    SDL_LockSurface(surf);
    SDL_FillRect(surf, &r, (Uint32)BLACK);
    //for (std::vector<SDL_Rect>::iterator it = points.begin(); it!=points.end();++it)
    //    SDL_FillRect(surf, &*it, reinterpret_cast<Uint32>(RED));
    SDL_UnlockSurface(surf);
    //SDL_UpdateRects(surf, 1, &r);
    ts.addExample(ex);
    ts.solve();
    fprintf(stderr, "\n(%f, %f) <- (%f, %f)\n", ex.in[0], ex.in[1], ex.out[0], ex.out[1]);
    ts.sub(ex.out, ex.in);
    fprintf(stderr, "(%f, %f) -> (%f, %f)\n", ex.in[0], ex.in[1], ex.out[0], ex.out[1]);
    r.w = r.h = 1;
    for (r.x = DIFF/2; r.x < surf->w; r.x+=DIFF) {
        for (r.y = DIFF/2; r.y < surf->h; r.y+=DIFF) {
#ifdef LATTICEPATTERN
            int mx = r.x+DIFF;
            int my = r.y+DIFF;
            for (int iter = 0; iter < 2; ++iter) {
                for (; (iter?r.x:r.y) < (iter?mx:my); (iter?r.x:r.y)++) {
#endif
            XYInterp::INVEC in;
            in[0] = 1.0*r.x/surf->w;
            in[1] = 1.0*r.y/surf->h;
            XYInterp::OUTVEC out;
            ts.sub(out, in);
            //fprintf(stderr, "(%f, %f) -> (%f, %f)\n", in[0], in[1], out[0], out[1]);
            SDL_Rect r2 = r;
            r2.x = static_cast<Sint16>(surf->w*out[0] + 0.5);
            r2.y = static_cast<Sint16>(surf->h*out[1] + 0.5);

            SDL_FillRect(surf, &r2, reinterpret_cast<Uint32>(YELLOW));
#ifdef LATTICEPATTERN
                }
                r.x = mx-DIFF;
                r.y = my-DIFF;
            }
#endif
        }
    }
    r.x = r.y = 0;
    r.w = surf->w;
    r.h = surf->h;
    SDL_UpdateRects(surf, 1, &r);
}


struct Quad {
    double xmin, xmax, ymin, ymax, xhalf, yhalf;

    Quad(double xn, double xx, double yn, double ym)
        :
    xmin(xn), xmax(xx), ymin(yn), ymax(ym), xhalf((xmin+xmax)*0.5), yhalf((ymin+ymax)*0.5)
    {

    }

    Quad nw() const {return Quad(xmin, xhalf, ymin, yhalf);}
    Quad ne() const {return Quad(xhalf, xmax, ymin, yhalf);}
    Quad se() const {return Quad(xhalf, xmax, yhalf, ymax);}
    Quad sw() const {return Quad(xmin, xhalf, yhalf, ymax);}
};

enum {
    W = 0x1,
    N = 0x2,
    E = 0x4,
    S = 0x8
};


/** Pending queue of points to "do" */
static std::deque<std::pair<Quad, int> > points;

/**
 * Arbitrarily divide up a rectangular region / Quad into subregions and queue
 * points at the centre the midpoints of the edges, such that no point is
 * ever done twice.
 */
static void divide(XYInterp &ts, const Quad &q, int quads = 0xf) {
    if (quads & W)
        doPoint(q.xmin, q.yhalf, ts);
    if (quads & N)
        doPoint(q.xhalf, q.ymin, ts);
    if (quads & E)
        doPoint(q.xmax, q.yhalf, ts);
    if (quads & S)
        doPoint(q.xhalf, q.ymax, ts);

    doPoint(q.xhalf, q.yhalf, ts);

    if ((int)(points.size()*4 + ts.dim()) < MAXDIM) {
        points.push_back(make_pair(q.nw(), quads&(N|S|E|W)));
        points.push_back(make_pair(q.ne(), quads&(N|E|S)));
        points.push_back(make_pair(q.se(), quads&(E|S)));
        points.push_back(make_pair(q.sw(), quads&(W|E|S)));
    }
}

/** Print out the usage to stderr */
static void usage(const char* argv0) {
    fprintf(stderr, "Usage: %s [<width=%d> <height=%f> [sigma=%d [numpoints=%d] ] ]\n",
            argv0,
            TARGET_SCREEN_WIDTH,
            TARGET_SCREEN_HEIGHT,
            SIGMA,
            MAXDIM);
}


/**
 * Callibaration test program main()
 */
int main(int argc, char **argv) {
    atexit(&SDL_Quit);
    if (argc > 2) {
        TARGET_SCREEN_WIDTH = atoi(argv[1]);
        TARGET_SCREEN_HEIGHT = atoi(argv[2]);
    }
    if (TARGET_SCREEN_WIDTH <= 0 ||
        TARGET_SCREEN_HEIGHT <= 0) {
        usage(argv[0]);
        return 1;
    }
    surf = makeSurface();

    if (!surf) {
        return 2;
    }

    if (argc > 3)
        SIGMA = atof(argv[3]);
    if (argc > 4)
        MAXDIM = atoi(argv[4]);

    usage(argv[0]);

    usleep(1000000);

//    if (argc > 1 && strcmp(argv[1], "-t") == 0)
//        return replay();

    /* four corners */
    XYInterp ts(SIGMA);
    //XYInterp ts("network.dat");
    double xmin = surf->w*FROM_EDGE;
    double xmax = surf->w - surf->w*FROM_EDGE;
    double ymin = surf->h*FROM_EDGE;
    double ymax = surf->h - surf->h*FROM_EDGE;

    doPoint(xmin, ymin, ts);
    doPoint(xmax, ymin, ts);
    doPoint(xmax, ymax, ts);
    doPoint(xmin, ymax, ts);

    /* midpoints */
    points.push_back(make_pair(Quad(xmin, xmax, ymin, ymax), N|S|E|W));

    while (!points.empty()) {
        divide(ts, points.front().first, points.front().second);
        points.pop_front();
    }

    {
        std::ofstream f("network.dat");
        f << ts << std::endl;
    }
    {
        std::ifstream f("network.dat");
        XYInterp xyi(f);
        std::cerr << xyi << std::endl;
    }

    usleep(5000000);

}

static SDL_Surface *makeSurface() {
    int videoFlags = 0;                  // Flags to pass to SDL_SetVideoMode
    const SDL_VideoInfo *videoInfo;	 // this holds some info about our display
    SDL_Surface *ret;

    /* initialize SDL */
    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        fprintf( stderr, "Video initialization failed: %s\n", SDL_GetError( ) );
	return 0;
    }
    if (!(videoInfo = SDL_GetVideoInfo( ))) {
        fprintf( stderr, "Video query failed: %s\n", SDL_GetError( ) );
        return 0;
    }
    /* the flags to pass to SDL_SetVideoMode */
//    videoFlags  = SDL_OPENGL;          /* Enable OpenGL in SDL */
//    videoFlags |= SDL_GL_DOUBLEBUFFER; /* Enable double buffering */
    videoFlags |= SDL_HWPALETTE;       /* Store the palette in hardware */
//    videoFlags |= SDL_RESIZABLE;       /* Enable window resizing */
//    if (TRY_FULLSCREEN)

    if (FULLSCREEN)
        videoFlags |= SDL_FULLSCREEN;

    /* This checks to see if surfaces can be stored in memory */
    videoFlags |= videoInfo->hw_available ? SDL_HWSURFACE : SDL_SWSURFACE;
    /* This checks if hardware blits can be done */
    if (videoInfo->blit_hw) videoFlags |= SDL_HWACCEL;
    /* Sets up OpenGL double buffering */
//    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

    /* Set up FSAA */
    /*
    if (RConfig::TARGET_FSAA > 0) {
        int fsaa = RConfig::TARGET_FSAA;
        while (fsaa > 0 && SDL_GL_SetAttribute (SDL_GL_SAMPLES_SIZE, fsaa)
            --fsaa;
        fprintf(stderr, "Using %dx FSAA\n", fsaa);
    }*/

    /* get a SDL surface */
    ret = SDL_SetVideoMode( TARGET_SCREEN_WIDTH, TARGET_SCREEN_HEIGHT, TARGET_SCREEN_BPP, videoFlags );
    if (!ret) {
        fprintf( stderr,  "Video mode set failed: %s\n", SDL_GetError( ) );
	return 0;
    }
    if (videoFlags & SDL_HWSURFACE) {
        fprintf(stderr, "Using hardware accelleration\n");
    }
    return ret;
}
