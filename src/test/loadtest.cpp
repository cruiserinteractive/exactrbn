#define RBFNET_READ_ONLY
#include "../rbn_solve.h"

#include <iostream>

int main() {
    XYInterp net("network.dat");
    for (int i = 0; i < 1500000; ++i) {
        XYInterp::INVEC in;
        in[0] = in[1] = i%30;
        XYInterp::OUTVEC out;
        net.sub(out, in);
//        std::cerr << out[0] << " " << out[1] << std::endl;
    }
}
