/* $Header: /home/tapted/cvstemp/cvs/exactrbn/src/lapack_leqn.h,v 7.2 2005/03/08 04:53:43 tapted Exp $ */
#ifndef LAPACK_LEQN_DOT_AITCH
#define LAPACK_LEQN_DOT_AITCH

/**\file lapack_leqn.h
 * A C++ wrapper for some (Fortran) linear equation system solvers from lapack.
 *
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * \version $Revision: 7.2 $
 * \date $Date$
 *
 * Link with
 *
 * g++ -o a.out files.o -lg2c -llapack -lblas
 */

extern "C" {
    /** Single-precision lapack linear equation solver (general matrix) -- see http://www.netlib.org/lapack/lug/node26.html -*/
    int sgesv_(int &a_size,
              int &c2,
              float *AT,
              int &pivot_size,
              int *pivot,
              float *rhs,
              int &b_size,
               int &ok);

    /** Double-precision lapack linear equation solver (general matrix) -- see http://www.netlib.org/lapack/lug/node26.html -*/
    int dgesv_(int &a_size,
              int &c2,
              double *AT,
              int &pivot_size,
              int *pivot,
              double *rhs,
              int &b_size,
              int &ok);
}

/* this seems to break stuff...
template <class LHS, class T>
static inline void fortran_transpose(LHS *out, const T *const in, int dim)
__attribute__((regparm(3),
               nonnull,
               nothrow,
               pure));
               */

//@{
/**
 * Solve the set of linear equations
 * \f[
 * \mathrm{A}\boldsymbol{x} = \boldsymbol{rhs}
 * \f]
 * and store the resulting \f$\boldsymbol{x}\f$ in \a rhs.
 *
 * \param rhs A vector of size \a dim, the right-hand side of the equation and the resulting \f$\boldsymbol{x}\f$
 * \param A A \a dim x \a dim square matrix in row-order -- the coefficients of \f$\boldsymbol{x}\f$
 * \param dim the dimension / order of the vector / matrix
 */
static inline int lapack_solve(double *const rhs, const double *const A, int dim)
__attribute__((regparm(3),
               nonnull(1, 2), ///< Pointer arguments should not be null
               nothrow,       ///< This is Fortran -- we never throw an exception
               unused         ///< This is inlined -- we don't care if we are not used
               /*,
               warn_unused_result*/));

static inline int lapack_solve(float *const rhs, const float *const A, int dim)
__attribute__((regparm(3),
               nonnull(1, 2), ///< Pointer arguments should not be null
               nothrow,       ///< This is Fortran -- we never throw an exception
               unused         ///< This is inlined -- we don't care if we are not used
               /*,
               warn_unused_result*/));
//@}
/**
 * Transpose a row-order matrix to column-order for Fortran.
 */
template <class LHS, class T>
static inline void fortran_transpose(LHS *out, const T *const in, int dim) {
    for (int i=0; i<dim; ++i) {
        const LHS *const lend = out + dim;
        const T *inp = in + i;
        for(; out < lend; ++out, inp+=dim)
            *out = *inp;
    }
}

static inline int lapack_solve(double *const rhs, const double *const A, int dim) {
    double *const AT = new double[dim*dim];
    int *const pivot = new int[dim];
    int c2 = 1;
    int ok;
    fortran_transpose(AT, A, dim);
    (void)dgesv_(dim, c2, AT, dim, pivot, rhs, dim, ok);
    delete[] AT;
    delete[] pivot;
    return ok;
}

static inline int lapack_solve(float *const rhs, const float *const A, int dim) {
    float *const AT = new float[dim*dim];
    int *const pivot = new int[dim];
    int c2 = 1;
    int ok;
    fortran_transpose(AT, A, dim);
    (void)sgesv_(dim, c2, AT, dim, pivot, rhs, dim, ok);
    delete[] AT;
    delete[] pivot;
    return ok;
}

#endif
